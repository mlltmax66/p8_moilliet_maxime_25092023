ToDoList
========

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/4e7a165f07ca4d8eb586992131f1b535)](https://app.codacy.com/gl/mlltmax66/p8_moilliet_maxime_25092023/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)

Project 8 : Improve an Existing Project.

Project repository : https://openclassrooms.com/projects/ameliorer-un-projet-existant-1

## Prerequisites

PHP version >= 8.1 and npm or yarn.

## Getting Started

1. Clone or download this repository :
```
    git clone https://gitlab.com/mlltmax66/p8_moilliet_maxime_25092023
```
2. Configure your environment variables (database connection) `.env.local` or `.env`.


3. Install the project dependencies with [Composer](https://getcomposer.org/download/) :
```
    composer install
```
4. Install the project dependencies with [Yarn](https://classic.yarnpkg.com/en/docs/install) :
```
    yarn install
```
or
```
    npm install
```
5. Create an asset build (using Webpack Encore) with [Yarn](https://classic.yarnpkg.com/en/docs/install) :
```
    yarn dev
```
or
```
    npm run dev
```
6. Create database if it does not already exist :
```
    php bin/console doctrine:database:create
```
7. Start migrations :
```
    php bin/console doctrine:migrations:migrate
```
8. Launch fixtures :
```
    php bin/console doctrine:fixtures:load
```
9. Server launch :
```
    php bin/console server:run
```
or
```
    php -S localhost:8000 -t public
```
10. Go to http://localhost:8000.
