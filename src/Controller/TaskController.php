<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\User;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/tasks', name: 'task_')]
class TaskController extends AbstractController
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    #[Route(path: '/', name: 'index', methods: [Request::METHOD_GET])]
    public function index(TaskRepository $taskRepository, Request $request): Response
    {
        $pagination = $taskRepository->findAllPaginate($request->query->getInt('page', 1), false);

        return $this->render('task/list.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    #[Route(path: '/done', name: 'index_done', methods: [Request::METHOD_GET])]
    public function indexDone(TaskRepository $taskRepository, Request $request): Response
    {
        $pagination = $taskRepository->findAllPaginate($request->query->getInt('page', 1), true);

        return $this->render('task/list_done.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    #[Route(path: '/create', name: 'new', methods: [Request::METHOD_GET, Request::METHOD_POST])]
    public function new(Request $request): Response
    {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $this->getUser();

            $task->setUser($user);

            $this->entityManager->persist($task);
            $this->entityManager->flush();

            $this->addFlash('success', 'La tâche a été bien été ajoutée.');

            return $this->redirectToRoute('task_index');
        }

        return $this->render('task/create.html.twig', ['form' => $form->createView()]);
    }

    #[Route(path: '/{id}/edit', name: 'edit', methods: [Request::METHOD_GET, Request::METHOD_POST])]
    public function edit(Task $task, Request $request): Response
    {
        $form = $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            $this->addFlash('success', 'La tâche a bien été modifiée.');

            return $this->redirectToRoute('task_index');
        }

        return $this->render('task/edit.html.twig', [
            'form' => $form->createView(),
            'task' => $task,
        ]);
    }

    #[Route(path: '/{id}/toggle', name: 'toggle', methods: [Request::METHOD_POST])]
    public function toggle(Task $task): RedirectResponse
    {
        $task->setIsDone(!$task->getIsDone());
        $this->entityManager->flush();

        if ($task->getIsDone()) {
            $this->addFlash(
                'success',
                sprintf('La tâche %s a bien été marquée comme faite.', $task->getTitle())
            );

            return $this->redirectToRoute('task_index');
        }

        $this->addFlash(
            'success',
            sprintf('La tâche %s a bien été marquée comme non-faite.', $task->getTitle())
        );

        return $this->redirectToRoute('task_index_done');
    }

    #[Route(path: '/{id}/delete', name: 'delete', methods: [Request::METHOD_POST])]
    #[IsGranted('task_delete', 'task')]
    public function delete(Task $task): RedirectResponse
    {
        $this->entityManager->remove($task);
        $this->entityManager->flush();

        $this->addFlash('success', 'La tâche a bien été supprimée.');

        return $this->redirectToRoute('task_index');
    }
}
