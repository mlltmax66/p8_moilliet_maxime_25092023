<?php

namespace App\DataFixtures;

use App\Factory\TaskFactory;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $adminUser = UserFactory::createOne([
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'roles' => ['ROLE_ADMIN'],
        ]);
        UserFactory::createMany(4);

        TaskFactory::createMany(10, function () use ($adminUser) {
            return [
                'user' => $adminUser,
            ];
        });
        TaskFactory::createMany(30, function () {
            return [
                'user' => UserFactory::random(),
            ];
        });
        TaskFactory::createMany(10);
    }
}
