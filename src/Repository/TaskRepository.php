<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @extends ServiceEntityRepository<Task>
 *
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private readonly PaginatorInterface $paginator)
    {
        parent::__construct($registry, Task::class);
    }

    /**
     * @return PaginationInterface<int, Task>
     */
    public function findAllPaginate(int $page, bool $isDone): PaginationInterface
    {
        $query = $this->createQueryBuilder('t')
            ->addSelect('u')
            ->leftJoin('t.user', 'u')
            ->where('t.isDone = (:isDone)')
            ->orderBy('t.createdAt', 'desc')
            ->setParameter('isDone', $isDone)
            ->getQuery();

        return $this->paginator->paginate($query, $page, 10);
    }
}
