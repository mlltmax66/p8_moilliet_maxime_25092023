<?php

namespace App\Factory;

use App\Entity\Task;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Task>
 *
 * @phpstan-method        Proxy<Task> create(array|callable $attributes = [])
 * @phpstan-method static Proxy<Task> createOne(array $attributes = [])
 * @phpstan-method static Proxy<Task> find(object|array|mixed $criteria)
 * @phpstan-method static Proxy<Task> findOrCreate(array $attributes)
 * @phpstan-method static Proxy<Task> first(string $sortedField = 'id')
 * @phpstan-method static Proxy<Task> last(string $sortedField = 'id')
 * @phpstan-method static Proxy<Task> random(array $attributes = [])
 * @phpstan-method static Proxy<Task> randomOrCreate(array $attributes = [])
 * @phpstan-method static RepositoryProxy<Task> repository()
 * @phpstan-method static list<Proxy<Task>> all()
 */
final class TaskFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function getDefaults(): array
    {
        return [
            'title' => self::faker()->words(rand(3, 7), true),
            'content' => self::faker()->paragraphs(rand(2, 5), true),
            'isDone' => self::faker()->boolean(10),
        ];
    }

    protected static function getClass(): string
    {
        return Task::class;
    }
}
