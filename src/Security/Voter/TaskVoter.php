<?php

namespace App\Security\Voter;

use App\Entity\Task;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class TaskVoter extends Voter
{
    public const TASK_DELETE = 'task_delete';

    protected function supports(string $attribute, mixed $subject): bool
    {
        if (self::TASK_DELETE != $attribute) {
            return false;
        }

        if (!$subject instanceof Task) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /** @var Task $task */
        $task = $subject;

        return match ($attribute) {
            self::TASK_DELETE => $this->canDelete($task, $user),
            default => throw new \LogicException('This code should not be reached!')
        };
    }

    private function canDelete(Task $task, User $user): bool
    {
        if (null === $task->getUser()) {
            return in_array('ROLE_ADMIN', $user->getRoles());
        }

        return $task->getUser() === $user;
    }
}
