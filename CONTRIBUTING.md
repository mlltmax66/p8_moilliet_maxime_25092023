# CONTRIBUTING

Thank you for considering contributing to this repository!

## Submitting a pull request

1. Fork and clone the repository.
2. Create a new branch: `git checkout -b my-branch-name`.
3. Make your changes (if you want to work on an issue, make sure you have been assigned that issue).
4. make sure that the linting is correct with this command :
```
    composer check
```
This command executes (PhpCsFixer, PhpStan and Rector).

5. Run the tests with command:
```
    php bin/phpunit
```

6. Commit your code.
7. Push to your fork and submit a pull request.
8. Y ou can now work on another issue to way for you PR to be merged.

> 💡 Please make sure your PR is readable for the most.
> Write a good commit message.
