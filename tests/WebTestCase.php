<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as BaseWebTestCase;

class WebTestCase extends BaseWebTestCase
{
    public function assertFormErrors(KernelBrowser $client, int $nbErrors, string $message = null): void
    {
        $this->assertEquals($nbErrors, $client->getCrawler()
            ->filter('.form-error')->count(), 'Form errors mismatch.');

        if (null != $message) {
            $this->assertEquals($message, $client->getCrawler()
                ->filter('.form-error')->text(), 'Text form errors mismatch.');
        }
    }

    public function assertAlert(KernelBrowser $client, string $type = 'success', string $message = null): void
    {
        $this->assertEquals(1, $client->getCrawler()
            ->filter('.alert-'.$type)->count(), 'Alert mismatch.');

        if (null != $message) {
            $this->assertEquals($message, $client->getCrawler()
                ->filter('.alert-'.$type)->text(), 'Text alert mismatch.');
        }
    }

    public function assertLoginRedirect(KernelBrowser $client): void
    {
        $this->assertEquals('/login', $client->getRequest()->getRequestUri());
    }

    public function assertH1(KernelBrowser $client, string $title): void
    {
        $crawler = $client->getCrawler();
        $this->assertEquals(
            $title,
            $crawler->filter('h1')->text(),
            '<h1> mismatch'
        );
    }
}
