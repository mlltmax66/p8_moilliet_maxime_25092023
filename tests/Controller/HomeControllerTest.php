<?php

namespace App\Tests\Controller;

use App\Entity\User;
use App\Factory\UserFactory;
use App\Tests\WebTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class HomeControllerTest extends WebTestCase
{
    use ResetDatabase;
    use Factories;

    public function testUserNotAccessIfNotLogin(): void
    {
        $client = static::createClient();
        $client->request('GET', '/');

        $this->assertResponseStatusCodeSame(302);

        $client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $this->assertLoginRedirect($client);
    }

    public function testIndex(): void
    {
        $user = UserFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);

        $client->request('GET', '/');
        $this->assertResponseStatusCodeSame(200);
        $this->assertH1($client, 'Tableau de bord');
    }
}
