<?php

namespace App\Tests\Controller;

use App\Entity\User;
use App\Factory\UserFactory;
use App\Tests\WebTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class UserControllerTest extends WebTestCase
{
    use ResetDatabase;
    use Factories;

    public function testNewUserNotAccessIfNotLogin(): void
    {
        $client = static::createClient();
        $client->request('GET', '/users/create');

        $this->assertResponseStatusCodeSame(302);

        $client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $this->assertLoginRedirect($client);
    }

    public function testNewtUserNotAccessIfNotAdmin(): void
    {
        $user = UserFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);

        $client->request('GET', '/users/create');
        $this->assertResponseStatusCodeSame(403);
    }

    public function testNewUser(): void
    {
        $adminUser = UserFactory::new()->create(['roles' => ['ROLE_ADMIN']]);
        self::ensureKernelShutdown();
        $client = static::createClient();
        /** @var User $adminUserObject */
        $adminUserObject = $adminUser->object();
        $client->loginUser($adminUserObject);

        $crawler = $client->request('GET', '/users/create');
        $this->assertResponseStatusCodeSame(200);
        $this->assertH1($client, 'Utilisateurs');

        $form = $crawler->selectButton('Ajouter')->form();
        $form->setValues([
            'user' => [
                'username' => 'Jane Doe',
                'email' => 'jane@doe.fr',
                'plainPassword' => [
                    'first' => 'password',
                    'second' => 'password',
                ],
                'roles' => 'ROLE_USER',
            ],
        ]);
        $client->submit($form);

        $this->assertFormErrors($client, 0);
        UserFactory::assert()->count(2);
        UserFactory::assert()->exists([
            'username' => 'Jane Doe',
            'email' => 'jane@doe.fr',
        ]);

        $this->assertResponseRedirects('/users/');
        $client->followRedirect();
        $this->assertAlert($client, 'success', 'L\'utilisateur a bien été ajouté.');
    }

    public function testNewUserUsernameNotBlankAndUnique(): void
    {
        UserFactory::new()->create(['username' => 'John Doe']);
        $adminUser = UserFactory::new()->create(['roles' => ['ROLE_ADMIN']]);
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $adminUserObject */
        $adminUserObject = $adminUser->object();
        $client->loginUser($adminUserObject);

        $crawler = $client->request('GET', '/users/create');
        $this->assertResponseStatusCodeSame(200);
        $this->assertH1($client, 'Utilisateurs');

        $form = $crawler->selectButton('Ajouter')->form();
        $formValues = [
            'user' => [
                'username' => '',
                'email' => 'jane@doe.fr',
                'plainPassword' => [
                    'first' => 'password',
                    'second' => 'password',
                ],
                'roles' => 'ROLE_USER',
            ],
        ];
        $form->setValues($formValues);
        $client->submit($form);
        $this->assertFormErrors($client, 1, 'Vous devez saisir un nom d\'utilisateur.');

        $formValues['user']['username'] = 'John Doe';
        $form->setValues($formValues);
        $client->submit($form);
        $this->assertFormErrors($client, 1, 'Le nom d\'utilisateur existe déjà.');
    }

    public function testNewUserEmailNotBlankAndValid(): void
    {
        $adminUser = UserFactory::new()->create(['roles' => ['ROLE_ADMIN']]);
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $adminUserObject */
        $adminUserObject = $adminUser->object();
        $client->loginUser($adminUserObject);

        $crawler = $client->request('GET', '/users/create');
        $this->assertResponseStatusCodeSame(200);
        $this->assertH1($client, 'Utilisateurs');

        $form = $crawler->selectButton('Ajouter')->form();
        $formValues = [
            'user' => [
                'username' => 'John Doe',
                'email' => '',
                'plainPassword' => [
                    'first' => 'password',
                    'second' => 'password',
                ],
                'roles' => 'ROLE_USER',
            ],
        ];
        $form->setValues($formValues);
        $client->submit($form);
        $this->assertFormErrors($client, 1, 'Vous devez saisir une adresse email.');

        $formValues['user']['email'] = 'janeDoe.fr';
        $form->setValues($formValues);
        $client->submit($form);
        $this->assertFormErrors($client, 1, 'Le format de l\'adresse n\'est pas correcte.');
    }

    public function testNewUserPasswordsSameAndMinLength(): void
    {
        $adminUser = UserFactory::new()->create(['roles' => ['ROLE_ADMIN']]);
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $adminUserObject */
        $adminUserObject = $adminUser->object();
        $client->loginUser($adminUserObject);

        $crawler = $client->request('GET', '/users/create');
        $this->assertResponseStatusCodeSame(200);
        $this->assertH1($client, 'Utilisateurs');

        $form = $crawler->selectButton('Ajouter')->form();
        $formValues = [
            'user' => [
                'username' => 'John Doe',
                'email' => 'jane@doe.fr',
                'plainPassword' => [
                    'first' => 'pass',
                    'second' => 'pas',
                ],
                'roles' => 'ROLE_USER',
            ],
        ];
        $form->setValues($formValues);
        $client->submit($form);
        $this->assertFormErrors($client, 1, 'Les deux mots de passe doivent correspondre.');

        $formValues['user']['plainPassword']['second'] = 'pass';
        $form->setValues($formValues);
        $client->submit($form);
        $this->assertFormErrors($client, 1, 'Le mot de passe doit contenir au moins 8 caractères.');
    }

    public function testEditUserNotAccessIfNotLogin(): void
    {
        $user = UserFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/users/'.$user->getId().'/edit');

        $this->assertResponseStatusCodeSame(302);

        $client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $this->assertLoginRedirect($client);
    }

    public function testEditUserNotAccessIfNotAdmin(): void
    {
        $userEdit = UserFactory::new()->create();
        $user = UserFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);

        $client->request('GET', '/users/'.$userEdit->getId().'/edit');
        $this->assertResponseStatusCodeSame(403);
    }

    public function testEditUser(): void
    {
        $user = UserFactory::new()->create();
        $adminUser = UserFactory::new()->create(['roles' => ['ROLE_ADMIN']]);
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $adminUserObject */
        $adminUserObject = $adminUser->object();
        $client->loginUser($adminUserObject);

        $crawler = $client->request('GET', '/users/'.$user->getId().'/edit');
        $this->assertResponseStatusCodeSame(200);
        $this->assertH1($client, 'Utilisateurs');

        $form = $crawler->selectButton('Modifier')->form();
        $form->setValues([
            'user' => [
                'username' => 'Jane Doe',
                'email' => 'jane@doe.fr',
                'plainPassword' => [
                    'first' => 'password',
                    'second' => 'password',
                ],
                'roles' => 'ROLE_USER',
            ],
        ]);
        $client->submit($form);

        $this->assertFormErrors($client, 0);
        UserFactory::assert()->count(2);
        UserFactory::assert()->exists([
            'username' => 'Jane Doe',
            'email' => 'jane@doe.fr',
        ]);

        $this->assertResponseRedirects('/users/');
        $client->followRedirect();
        $this->assertAlert($client, 'success', 'L\'utilisateur a bien été modifié.');
    }

    public function testAdminUserCanDeleteUser(): void
    {
        $user = UserFactory::new()->create(['roles' => ['ROLE_ADMIN']]);
        $userDelete = UserFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);
        $client->request('POST', '/users/'.$userDelete->getId().'/delete');

        UserFactory::assert()->count(1);
        $this->assertResponseRedirects('/users/');
        $client->followRedirect();
        $this->assertAlert(
            $client,
            'success', 'L\'utilisateur a bien été supprimé.',
        );
    }
}
