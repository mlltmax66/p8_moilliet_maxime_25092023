<?php

namespace App\Tests\Controller;

use App\Entity\User;
use App\Factory\TaskFactory;
use App\Factory\UserFactory;
use App\Tests\WebTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class TaskControllerTest extends WebTestCase
{
    use ResetDatabase;
    use Factories;

    public function testIndexUserNotAccessIfNotLogin(): void
    {
        $client = static::createClient();
        $client->request('GET', '/tasks/');

        $this->assertResponseStatusCodeSame(302);

        $client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $this->assertLoginRedirect($client);
    }

    public function testIndex(): void
    {
        $user = UserFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);

        $client->request('GET', '/tasks/');
        $this->assertResponseStatusCodeSame(200);
        $this->assertH1($client, 'Tâches');
    }

    public function testIndexDoneUserNotAccessIfNotLogin(): void
    {
        $client = static::createClient();
        $client->request('GET', '/tasks/done');

        $this->assertResponseStatusCodeSame(302);

        $client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $this->assertLoginRedirect($client);
    }

    public function testIndexDone(): void
    {
        $user = UserFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);

        $client->request('GET', '/tasks/done');
        $this->assertResponseStatusCodeSame(200);
        $this->assertH1($client, 'Tâches terminées');
    }

    public function testNewTaskUserNotAccessIfNotLogin(): void
    {
        $client = static::createClient();
        $client->request('GET', '/tasks/create');
        $this->assertResponseStatusCodeSame(302);

        $client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $this->assertLoginRedirect($client);
    }

    public function testNewTask(): void
    {
        $user = UserFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);

        $crawler = $client->request('GET', '/tasks/create');
        $this->assertResponseStatusCodeSame(200);
        $this->assertH1($client, 'Tâches');

        $form = $crawler->selectButton('Ajouter')->form();
        $form->setValues([
            'task' => [
                'title' => 'Title',
                'content' => 'Content',
            ],
        ]);
        $client->submit($form);

        $this->assertFormErrors($client, 0);
        TaskFactory::assert()->count(1);
        TaskFactory::assert()->exists([
            'title' => 'Title',
            'content' => 'Content',
            'isDone' => false,
        ]);

        $this->assertResponseRedirects('/tasks/');
        $client->followRedirect();
        $this->assertAlert($client, 'success', 'La tâche a été bien été ajoutée.');
    }

    public function testNewTaskTitleNotBlank(): void
    {
        $user = UserFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);

        $crawler = $client->request('GET', '/tasks/create');
        $this->assertResponseStatusCodeSame(200);
        $this->assertH1($client, 'Tâches');

        $form = $crawler->selectButton('Ajouter')->form();
        $form->setValues([
            'task' => [
                'title' => '',
                'content' => 'Content',
            ],
        ]);
        $client->submit($form);

        $this->assertFormErrors($client, 1, 'Vous devez saisir un titre.');
    }

    public function testNewTaskContentNotBlank(): void
    {
        $user = UserFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);

        $crawler = $client->request('GET', '/tasks/create');
        $this->assertResponseStatusCodeSame(200);
        $this->assertH1($client, 'Tâches');

        $form = $crawler->selectButton('Ajouter')->form();
        $form->setValues([
            'task' => [
                'title' => 'Title',
                'content' => '',
            ],
        ]);
        $client->submit($form);

        $this->assertFormErrors($client, 1, 'Vous devez saisir du contenu.');
    }

    public function testEditUserNotAccessIfNotLogin(): void
    {
        $task = TaskFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('GET', '/tasks/'.$task->getId().'/edit');
        $this->assertResponseStatusCodeSame(302);

        $client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $this->assertLoginRedirect($client);
    }

    public function testEditTask(): void
    {
        $user = UserFactory::new()->create();
        $task = TaskFactory::createOne(['user' => $user, 'isDone' => false]);
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);

        $crawler = $client->request('GET', '/tasks/'.$task->getId().'/edit');
        $this->assertResponseStatusCodeSame(200);
        $this->assertH1($client, 'Tâches');

        $form = $crawler->selectButton('Modifier')->form();
        $form->setValues([
            'task' => [
                'title' => 'Title',
                'content' => 'Content',
            ],
        ]);
        $client->submit($form);

        $this->assertFormErrors($client, 0);
        TaskFactory::assert()->count(1);
        TaskFactory::assert()->exists([
            'title' => 'Title',
            'content' => 'Content',
            'isDone' => false,
            'user' => $user,
        ]);

        $this->assertResponseRedirects('/tasks/');
        $client->followRedirect();
        $this->assertAlert($client, 'success', 'La tâche a bien été modifiée.');
    }

    public function testEditTaskUserNotUpdate(): void
    {
        $user = UserFactory::new()->create();
        $userTask = UserFactory::new()->create();
        $task = TaskFactory::createOne(['user' => $userTask, 'isDone' => false]);
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);

        $crawler = $client->request('GET', '/tasks/'.$task->getId().'/edit');
        $this->assertResponseStatusCodeSame(200);
        $this->assertH1($client, 'Tâches');

        $form = $crawler->selectButton('Modifier')->form();
        $form->setValues([
            'task' => [
                'title' => 'Title',
                'content' => 'Content',
            ],
        ]);
        $client->submit($form);

        $this->assertFormErrors($client, 0);
        TaskFactory::assert()->count(1);
        TaskFactory::assert()->exists([
            'title' => 'Title',
            'content' => 'Content',
            'isDone' => false,
            'user' => $userTask,
        ]);
        TaskFactory::assert()->notExists([
            'title' => 'Title',
            'content' => 'Content',
            'isDone' => false,
            'user' => $user,
        ]);

        $this->assertResponseRedirects('/tasks/');
        $client->followRedirect();
        $this->assertAlert($client, 'success', 'La tâche a bien été modifiée.');
    }

    public function testToggleUserNotAccessIfNotLogin(): void
    {
        $task = TaskFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('POST', '/tasks/'.$task->getId().'/toggle');
        $this->assertResponseStatusCodeSame(302);

        $client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $this->assertLoginRedirect($client);
    }

    public function testToggle(): void
    {
        $user = UserFactory::new()->create();
        $task = TaskFactory::new()->create(['isDone' => false]);
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);
        $client->request('POST', '/tasks/'.$task->getId().'/toggle');

        TaskFactory::assert()->exists([
            'title' => $task->getTitle(),
            'content' => $task->getContent(),
            'isDone' => true,
        ]);
        $this->assertResponseRedirects('/tasks/');
        $client->followRedirect();
        $this->assertAlert(
            $client,
            'success', 'La tâche '.$task->getTitle().' a bien été marquée comme faite.',
        );

        $client->request('POST', '/tasks/'.$task->getId().'/toggle');
        TaskFactory::assert()->exists([
            'title' => $task->getTitle(),
            'content' => $task->getContent(),
            'isDone' => false,
        ]);
        $this->assertResponseRedirects('/tasks/done');
        $client->followRedirect();
        $this->assertAlert(
            $client,
            'success', 'La tâche '.$task->getTitle().' a bien été marquée comme non-faite.',
        );
    }

    public function testDeleteUserNotAccessIfNotLogin(): void
    {
        $task = TaskFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        $client->request('POST', '/tasks/'.$task->getId().'/delete');
        $this->assertResponseStatusCodeSame(302);

        $client->followRedirect();
        $this->assertResponseStatusCodeSame(200);
        $this->assertLoginRedirect($client);
    }

    public function testUserCanDeleteTheirTask(): void
    {
        $user = UserFactory::new()->create();
        $task = TaskFactory::new()->create(['user' => $user]);
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);
        $client->request('POST', '/tasks/'.$task->getId().'/delete');

        TaskFactory::assert()->count(0);
        $this->assertResponseRedirects('/tasks/');
        $client->followRedirect();
        $this->assertAlert(
            $client,
            'success', 'La tâche a bien été supprimée.',
        );
    }

    public function testUserCannotDeleteATaskThatDoesNotBelongToHim(): void
    {
        $user = UserFactory::new()->create();
        $otherUser = UserFactory::new()->create();
        $task = TaskFactory::new()->create(['user' => $otherUser]);
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $user->object();
        $client->loginUser($userObject);
        $client->request('POST', '/tasks/'.$task->getId().'/delete');

        TaskFactory::assert()->count(1);
    }

    public function testAdminUserCanDeleteATaskThatDoesNotBelongToHimIfTaskIsNotLinked(): void
    {
        $adminUser = UserFactory::new()->create(['roles' => ['ROLE_ADMIN']]);
        $task = TaskFactory::new()->create();
        self::ensureKernelShutdown();

        $client = static::createClient();
        /** @var User $userObject */
        $userObject = $adminUser->object();
        $client->loginUser($userObject);
        $client->request('POST', '/tasks/'.$task->getId().'/delete');

        TaskFactory::assert()->count(0);
        $this->assertResponseRedirects('/tasks/');
        $client->followRedirect();
        $this->assertAlert(
            $client,
            'success', 'La tâche a bien été supprimée.',
        );
    }
}
